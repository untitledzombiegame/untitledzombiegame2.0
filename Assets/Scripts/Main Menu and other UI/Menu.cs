﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public Button StartButton;
    public Button InstructionsButton;
    public Button QuitButton;
    public Button BackButton;
    public GameObject StartB;
    public GameObject InstrB;
    public GameObject QuitB;
    public GameObject BackB;
    public GameObject InstructionsText;
    bool Instructions = false;
    // Start is called before the first frame update
    void Start()
    {
        StartButton.onClick.AddListener(StartGame);
        InstructionsButton.onClick.AddListener(Options);
        QuitButton.onClick.AddListener(Quit);
        BackButton.onClick.AddListener(Back);
    }

    // Update is called once per frame
    void Update()
    {
        if (Instructions)
        {
            StartB.SetActive(false);
            InstrB.SetActive(false);
            QuitB.SetActive(false);
            BackB.SetActive(true);
            InstructionsText.SetActive(true);
        }
        else
        {
            StartB.SetActive(true);
            InstrB.SetActive(true);
            QuitB.SetActive(true);
            BackB.SetActive(false);
            InstructionsText.SetActive(false);
        }
    }

    void StartGame()
    {
        Debug.Log("Start");
        SceneManager.LoadScene("MainLevel");
    }

    void Options()
    {
        Debug.Log("Options");
        Instructions = !Instructions;
    }

    void Quit()
    {
        Debug.Log("Quitting");
        Application.Quit();
    }

    void Back()
    {
        Instructions = !Instructions;
    }
}
