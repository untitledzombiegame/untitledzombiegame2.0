﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public GameObject Player;
    public static bool BarEmpty = false;
    bool Recovering = false;
    public RectTransform bartransform;
    public Image HealthImage;
    public static bool isAttacked = false;
    float timer;
    Vector3 SubstractedVector = new Vector3(12.7f, 0, 0);
    Vector3 InitialVector = new Vector3(249.5f, -43.8f, 0);
    public static  int ActualHealth = 4;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindWithTag("Player");
        //-425 -> -437.7
    }

    // Update is called once per frame
    void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.R))
        {
            isAttacked = true;           
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            isAttacked = false;
        }
        */
        if ((HealthImage.rectTransform.localScale.x > 0) && (isAttacked))
        {
            Recovering = false;
            HealthImage.rectTransform.localScale -= new Vector3(0.25f, 0, 0);
            InitialVector -= SubstractedVector;
            bartransform.anchoredPosition = InitialVector;
            isAttacked = false;
            timer = Time.time;
            ActualHealth = ActualHealth-1;
        }
        if (Time.time - timer > 10)
            Recovering = true;
        if ((HealthImage.rectTransform.localScale.x < 5) && (Recovering))
        {
            HealthImage.rectTransform.localScale += new Vector3(0.25f, 0, 0);
            InitialVector += SubstractedVector;
            bartransform.anchoredPosition = InitialVector;
            //Sets  health to 4 to stop health stacking
            ActualHealth = 4;
        }
        if(ActualHealth <= 0)
        {
            Destroy(Player);
        }
    }
}
