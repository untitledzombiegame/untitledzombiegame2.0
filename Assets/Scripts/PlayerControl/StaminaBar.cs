﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public static bool BarEmpty = false;
    public RectTransform bartransform;
    public Image StaminaImage;
    Vector3 SubstractedVector = new Vector3(0.125f, 0, 0);
    Vector3 InitialVector = new Vector3(174.4f, -30.29999f, 0);
    //+0.020
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if ((PlayerMove.isSprinting) && (StaminaImage.transform.localScale.x > 0))
        {
            StaminaImage.transform.localScale -= new Vector3(0.25f, 0, 0);
            BarEmpty = false;
            InitialVector -= SubstractedVector;
            bartransform.anchoredPosition = InitialVector;
        }
        else if (StaminaImage.transform.localScale.x <= 0)
        {
            BarEmpty = true;
        }
        if ((!PlayerMove.isSprinting) && (StaminaImage.transform.localScale.x < 350))
        {
            StaminaImage.transform.localScale += new Vector3(0.25f, 0, 0);
            BarEmpty = false;
            InitialVector += SubstractedVector;
            bartransform.anchoredPosition = InitialVector;
        }
    }
}
