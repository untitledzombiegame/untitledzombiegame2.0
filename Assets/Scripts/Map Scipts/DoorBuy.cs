﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBuy : MonoBehaviour
{
    public int Score = 0;
    public GameObject Door;
    public bool RemoveDoor = false;
    
    // Update is called once per frame
    void Update()
    {
        if ( Score >= 700 & RemoveDoor == true )
        {
          Object.Destroy(Door);
            Score = Score - 700;
        }
    }

   void OnMouseDown()
    {
        RemoveDoor = true; 
    }
}
