﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject Zombie;
    public float NumberOfObjects = 10;
    public float TimeUntilSpawn = 0f;
    bool Spawn = false;
    float cooldown = 0;
    // Start is called before the first frame update
    void Start()
    {
        //SpawnObjects();
    }

    // Update is called once per frame
    void Update()
    {
        TimeUntilSpawn += Time.deltaTime;
        if (TimeUntilSpawn >= 20)
        {
            SpawnObjects();
            //Take this out for fun times
            TimeUntilSpawn = 0;
            //Spawn = true;
        }

        //if (Spawn == true)
        //{
        //    SpawnObjects();
        //    cooldown += Time.deltaTime;
        //    TimeUntilSpawn = 0;
        //}

        //if (cooldown >= 5)
        //{
        //    Spawn = false;
        //    cooldown = 0;
        //}
    }

    void SpawnObjects()
   {
     Instantiate(Zombie, transform.position, transform.rotation);
   }
}
