﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHealth : MonoBehaviour
{
    float  Counter;
    int Rounds = 1;
   public int BaseHealth = 100;
    float Health;
    // Start is called before the first frame update
    void Start()
    {
        AddHealth(); 
    }

    // Update is called once per frame
    void Update()
    {
        //Keeps Counter adding up
        Counter =+ Time.deltaTime;
        if (Health <= 0)
        {
            Die();
        }
    }
    // Call in to kill zombie
    void Die()
    {
        Destroy(gameObject);
    }
    //Call in to add new round
    void AddHealth()
    {
        Health = Rounds * BaseHealth;
    }

    void RoundCounter()
    {
        // Every 5 minutes rounds increase and then resets back to 0
        if(Counter >= 300)
        {
            Rounds =+ 1;
            AddHealth();
            Counter = 0;
        }
    }

    public void TakeDamage(int amount)
    {
        BaseHealth -= amount;
        if(BaseHealth <=0)
        {
            Die();
        }
    }



}
