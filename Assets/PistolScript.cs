﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PistolScript : MonoBehaviour
{
    #region  Gun Variables
    public int damage = 10;
    public int  Range = 100;

    public int maxAmmo;
    private int currentAmmo;
    public float reloadTime = 3f;
    public bool isRelaoding = false;
    public bool isShooting = false;
    public float shootTime = 2f;
    public Camera FPScam;

    public Animator animator;

    public ParticleSystem MuzzleFlash;
    #endregion

     void Start()
     {
        // this is how the quateine function is called
        
        currentAmmo = maxAmmo;
     }


    void Update()
    {
        // if we are reloading we want to do nothing 
        if(isRelaoding)
        {
            return;
        }
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            //code will stop here 
            return;
        }
        if (Input.GetButtonDown("Fire1"))
        {
            PistolShoot();
           
        }
     
    }


    public void  PistolShoot()
    {
        MuzzleFlash.Play();
      
        currentAmmo--;
       
        

        // raycasting will be used for the shooting
        // basically the gun will shoot and invisible ray which will gather information if the ray hits an object 
        // if a target is hit then it can take damage otherwise we have shot into thin air therfor we do nothing 

        // stores the information on what we hit with the ray 
        RaycastHit hit;
        // shots out the ray
        // shoots the array from the foward positon of the camera as well as stores the information in hit and displays the range 
      
        
        if (Physics.Raycast(FPScam.transform.position, FPScam.transform.forward, out hit, Range))
        {
         // isShooting = true;
           // yield return new WaitForSeconds(shootTime);


            // checks to see what was hit and loges it in the console 
            Debug.Log(hit.transform.name);
          //  yield return new WaitForSeconds(shootTime);
           // isShooting = false;

            // Calls the ZombieHealth script through the ray cast so that 
            // when we hit the zombie it can be damaged
            // also stored this in a variable to make things easier
            ZombieHealth ZombieTarget =  hit.transform.GetComponent<ZombieHealth>();
            //Checks to see if we have the ZombieHealth Component
            if (ZombieTarget !=null)
            {
                ZombieTarget.TakeDamage(damage);
            }
        
        }
       
      

    }

    // this function is now an IEnumerator, the function is an quateiness which means the function can be passed at anytime 
    IEnumerator Reload()
    {
        isRelaoding = true;
        Debug.Log("Reloading");

        animator.SetBool("Reloading", true);
        // basically the time it takes to reload the wepon 
        yield return new WaitForSeconds(reloadTime - .5f);

        animator.SetBool("Reloading", false);
       yield return new WaitForSeconds(.5f);
        currentAmmo = maxAmmo;
        isRelaoding = false;



    }

}
